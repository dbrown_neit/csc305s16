import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;
public class FactorAnalyzerTest {
	private PrimeFactorizer pf;
	private FactorAnalyzer fa;
	@Before
	public void beforeEachTest() {
		// create a new FactorAnalyzer for testing
		fa = new FactorAnalyzer(); // regular Java
		// create a mock PrimeFactorizer
		pf = mock(PrimeFactorizer.class); // Mockito method
		// use the mock object
		fa.setMyPrimeFactorizer(pf); // regular Java
	}
	@After
	public void afterEachTest() {
		//make sure we used the mock PrimeFactorizer
		verify(pf).getFactors(anyInt()); // Mockito method
	}
	
	@Test
	public void test1() {
		// describe the expected behavior of this object 
		// in the context of this test
		when(pf.getFactors(1)).thenReturn(new int[] {}); // this is Mockito
		assertTrue(fa.isPerfectSquare(1)); // this is JUnit
	}
	@Test
	public void test2() {
		when(pf.getFactors(2)).thenReturn(new int[] {2});
		assertFalse(fa.isPerfectSquare(2));
	}
	@Test
	public void test6() {
		when(pf.getFactors(6)).thenReturn(new int[] {2,3});
		assertFalse(fa.isPerfectSquare(62));
	}
}
