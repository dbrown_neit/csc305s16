
public class FactorAnalyzer {
	private PrimeFactorizer myPrimeFactorizer;
	
	
	/**
	 * Will instantiate a new PrimeFactorizer if one has not yet
	 * been supplied
	 * @return the current PrimeFactorizer
	 */
	public PrimeFactorizer getMyPrimeFactorizer() {
		if(null==myPrimeFactorizer) {
			myPrimeFactorizer = new PrimeFactorizer();
		}
		return myPrimeFactorizer;
	}


	/**
	 * Method provided to allow dependency injection and make the code 
	 * more testable.
	 * 
	 * @param myPrimeFactorizer
	 */
	public void setMyPrimeFactorizer(PrimeFactorizer myPrimeFactorizer) {
		this.myPrimeFactorizer = myPrimeFactorizer;
	}

	/**
	 * To make things interesting, must evaluate using 
	 * the prime factor list returned by myPrimeFactorizer.
	 * Specifically disallowed: computing the square root; multiplication
	 * @param n
	 * @return true if n is a perfect square
	 */
	public boolean isPerfectSquare(int n) {
		int[] factors = getMyPrimeFactorizer().getFactors(n);
		/** @TODO: implement a real algorithm for this */
		return factors.length%2==0;
	}
}
